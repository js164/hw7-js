const test = ['hello', 'world', 23, '23', null, {a:1}, [1, 2, 3], true, false]

function  filterBy (array, type) {
  return array.filter(item => {
    if (type === 'object') {
      return typeof item !== type || Array.isArray(item) || item === null;
    } else if (type === 'null') {
      return item !== null;
    } else if (type === 'array') {
      return !Array.isArray(item)
    } else {
      return typeof item !== type;
    }
  });
}

console.log(test);
// console.log(filterBy(test, 'string'));
// console.log(filterBy(test, 'number'));
// console.log(filterBy(test, 'array'));
// console.log(filterBy(test, 'boolean'));
// console.log(filterBy(test, 'object'));
// console.log(filterBy(test, 'null'));
